import exception.GameException;

import java.util.*;
import java.util.stream.Collectors;


public class WordFrequencyGame {
    public String calculateWordsFrequency(String words) {
        try {
            List<Input> wordsSplit = splitWords(words);
            List<Input> wordsSplitAndPolymerized = countWordsFrequency(wordsSplit);
            return generateWordFrequencyReport(wordsSplitAndPolymerized);
        } catch (RuntimeException exception) {
            throw new GameException();
        }
    }

    private List<Input> splitWords(String words) {
        return Arrays
                .stream(words.split("\\s+"))
                .map(stringSplit -> new Input(stringSplit, 1))
                .collect(Collectors.toList());
    }

    private String generateWordFrequencyReport(List<Input> wordsSplitAndPolymerized) {
        StringJoiner joiner = new StringJoiner("\n");
        wordsSplitAndPolymerized.forEach(word -> joiner.add(word.getValue() + " " + word.getWordCount()));
        return joiner.toString();
    }


    private List<Input> countWordsFrequency(List<Input> wordsSplit) {
        Map<String, Long> mapFromWordSplitToNumber = wordsSplit
                .stream()
                .collect(Collectors.groupingBy(Input::getValue, Collectors.counting()));
        return mapFromWordSplitToNumber
                .entrySet()
                .stream()
                .map(wordSplitEntry -> new Input(
                        wordSplitEntry.getKey(),
                        Math.toIntExact(wordSplitEntry.getValue())))
                .sorted((word1, word2) -> word2.getWordCount() - word1.getWordCount())
                .collect(Collectors.toList());
    }
}
