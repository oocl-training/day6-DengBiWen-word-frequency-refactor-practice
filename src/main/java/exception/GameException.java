package exception;

public class GameException extends RuntimeException {
    public GameException() {
        super("game error");
    }
}
