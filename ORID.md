## ORID

### O (Objective)
What did we learn today? What activities did you do? What scenes have impressed you?
```
+ Design Pattern
    1. Strategy Pattern
        include Context、 Strategy
    2. Observer Pattern
        include Observer、 Subject
    3. Command Pattern
        include Invoker、 Command、 Receiver
+ Code Smell
```
### R (Reflective)
Please use one word to express your feelings about today's class.
```
full
```
### I (Interpretive)
What do you think about this? What was the most meaningful aspect of this activity?
```
If there are no requirements for extension and modification, it would be acceptable.
If we have to use the Observer pattern, you need to add more complex classes and codes, which is an over design.
If the requirements change frequently, we need to frequently modify the code, which violates the Open–closed principle.
So we can use Design Pattern to reduce decoupl our code.
```
### D (Decisional)
Where do you most want to apply what you have learned today? What changes will you make?
```
Understanding code smell can help us avoid write code smell as much as possible. 
Also I will try my best to reduce my code smell after refatoring.
```